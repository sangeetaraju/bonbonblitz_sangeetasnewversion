﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    private float originalY;
    public float bobStrength;
    public LayerMask m_TankMask;
    // Use this for initialization
    void Start () {
        originalY = transform.position.y;
        bobStrength = 0.25f;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, originalY + (Mathf.Sin(Time.time) * bobStrength), transform.position.z);
		
	}


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.


        TankHealth targetHealth = other.GetComponent<TankHealth>();
        if (targetHealth)
        {
            targetHealth.GetHealth();
            gameObject.SetActive(false);
        }
        

    }
}
